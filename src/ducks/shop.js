import {shopItems} from "../helpers/shop-items";
import {cloneDeep} from "lodash";
import axios from "axios";

const UPDATE_SHOP = 'UPDATE_SHOP';
const UPDATE_BASKET = 'UPDATE_BASKET';
const UPDATE_USERS = 'UPDATE_USERS';

export default function shop(state = {users: [], items: shopItems.sort((a, b) => b.price - a.price), basket: []}, action) {
    switch (action.type) {
        case UPDATE_SHOP:
            return {...state, items: action.items};
        case UPDATE_BASKET:
            return {...state, basket: action.basket};
        case UPDATE_USERS:
            return {...state, users: action.users};
        default:
            return state
    }
}

export function incrementItem(index) {
    return (dispatch, state) => {
        let {items} = state().shop;
        items = [...items];
        items[index].counts = items[index].counts + 1;
        dispatch({type: UPDATE_SHOP, items})
    }
}

export function decrementItem(index) {
    return (dispatch, state) => {
        let {items} = state().shop;
        items = [...items];
        if (items[index].counts !== 0) {
            items[index].counts = items[index].counts - 1;
            dispatch({type: UPDATE_SHOP, items})
        }
    }
}

export function sortItems(sort) {
    return (dispatch, state) => {
        let {items} = state().shop;
        items = [...items];
        if (sort === 'desc') {
            items.sort((a, b) => a.price - b.price);
        } else {
            items.sort((a, b) => b.price - a.price);
        }
            dispatch({type: UPDATE_SHOP, items})
    }
}


export function searchItem(search) {
    return (dispatch) => {
        const items = shopItems.filter(item => item.title.toLowerCase().includes(search.toLowerCase()))
        dispatch({type: UPDATE_SHOP, items})
    }
}



export function setBasket(index) {
    return (dispatch, state) => {
        let {items, basket} = state().shop;
        items = cloneDeep(items);
        basket = cloneDeep(basket);
        let includeBasketItem;
        includeBasketItem = basket.find(e => e.id === items[index].id);
        if (includeBasketItem) {
            includeBasketItem.counts = includeBasketItem.counts + [...items][index].counts;
        } else {
            const newItems = cloneDeep(items);
            basket.push(newItems[index]);
            }
        items[index].counts = 0;
        dispatch({type: UPDATE_BASKET, basket})
        dispatch({type: UPDATE_SHOP, items})
    }
}


export function incrementBasketItem(index) {
    return (dispatch, state) => {
        let {basket} = state().shop;
        basket = [...basket];
        basket[index].counts = basket[index].counts + 1;
        dispatch({type: UPDATE_BASKET, basket})
    }
}

export function decrementBasketItem(index) {
    return (dispatch, state) => {
        let {basket} = state().shop;
        basket = [...basket];
        if (basket[index].counts !== 0) {
            basket[index].counts = basket[index].counts - 1;
            dispatch({type: UPDATE_BASKET, basket})
        }
    }
}

export function getUsers(cb) {
    return async (dispatch) => {
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/posts', {params: {data: 123}})
        dispatch({type: UPDATE_USERS, users: data})
        cb();
    }
}

