import React, {Component} from "react";
import {connect} from "react-redux";
import './shopItemBody.css'

class ShopItemBody extends Component {


    render() {
        const {items, match} = this.props;
        const item = items.find(item => item.id === +match.params.itemId);
        return (
            <div className='gameList'>
                <div className='title'>
                    {item.title}
                </div>
                <img alt='' className='image' src={require('.' + item.img)}/>
                <div className='fullDescription'>
                    <h6>Описание игры:</h6> <p>{item.fullDescription}</p>
                </div>
                <div className='description'>
                    <h6>Системные требывания:</h6> {item.Description}
                </div>
                <div className='price'>
                    <h6>Цена:</h6> {item.price}
                </div>
                <div className='currency'>
                    <h6>Валюта:</h6> {item.currency}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.shop.items,
    }
};

export default connect(mapStateToProps)(ShopItemBody);