import React, {Component} from 'react';
import './index.css'

class Login extends Component {
    state = {
        email: '',
        password: '',
        remember: false
    };

    registration = () => {
        this.props.history.push('/registration')
    }

    handleChange = (event) => {
        const input = event.target;
        const value = input.type === 'checkbox' ? input.checked : input.value;
        this.setState({[input.name]: value});
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const {history} = this.props;
        const {email, password, remember} = this.state;
        localStorage.setItem('remember', remember);
        localStorage.setItem('email', remember ? email : '');
        localStorage.setItem('password', remember ? password : '');
        history.push('/shop');
    };

    render() {
        const {email, password, remember} = this.state;
        return (
            <div className="login-title">
                <form onSubmit={this.handleFormSubmit} className="formStyle">
                    <p className="text">Вход в личный кабинет</p>
                    <label>
                        <div className="form-input">
                            <input
                                type='email'
                                name='email'
                                placeholder='Введите почту'
                                value={email}
                                onChange={this.handleChange}/>
                            <input
                                type='password'
                                name='password'
                                placeholder='Введите пароль'
                                value={password}
                                onChange={this.handleChange}/>
                        </div>
                        <div>Запомнить?
                            <input
                                type='checkbox'
                                name='remember'
                                checked={remember}
                                onChange={this.handleChange}/>
                        </div>
                        <div className="buttonPath">
                            <button className="buttonEnter" type="submit">Войти</button>
                            <button onClick={this.registration}>Регистрация</button>
                        </div>
                    </label>
                </form>
            </div>
        );
    }
}

export default Login;