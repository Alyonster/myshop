import React, {useRef} from 'react';
import './registration.css'
import {useForm} from "react-hook-form";


const Registration = () => {
    const { register, errors, handleSubmit, watch } = useForm({});
    const password = useRef({});
    password.current = watch("password", "");
    const onSubmit = async data => {
        alert(JSON.stringify(data));
    };


        return (
            <div className='registration_title'>
                <form onSubmit={e => e.preventDefault()} className='formReg' action="#" method="#">
                    <h2>Регистрация</h2>
                    <input
                        type='text'
                        name='firstname'
                        placeholder='Имя *'
                        ref={register({
                            required: 'Укажите имя'
                        })}
                    />
                    {errors.firstname && <p className='errorMessage'>{errors.firstname.message}</p>}
                    <input
                        type='text'
                        name='lastname'
                        placeholder='Фамилия'
                        ref={register}
                    />
                    <input
                        type='email'
                        name='email'
                        placeholder='Эл. почта *'
                        ref={register({
                            required: 'Укажите почту',
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "Некорректная почта"
                            }
                        })}
                    />
                    {errors.email && <p className='errorMessage'>{errors.email.message}</p>}
                    <input
                        type='password'
                        name='password'
                        placeholder='Придумайте пароль *'
                        ref={register({
                            required: "Укажите пароль",
                            minLength: {
                                value: 8,
                                message: "Пароль должен состоять не менее 8 символов"
                            },
                            maxLength: {
                                value: 20,
                                message: "Пароль не должен превышать 20 символов"
                            }
                        })}
                    />
                    {errors.password && <p className='errorMessage'>{errors.password.message}</p>}
                    <input
                        type='password'
                        name='password_repeat'
                        placeholder='Повторите пароль *'
                        ref={register({
                            validate: value =>
                                value === password.current || "Пароль не совпадает"
                        })}
                    />
                    {errors.password_repeat && <p className='errorMessage'>{errors.password_repeat.message}</p>}
                    <input
                        type='tel'
                        name='tel'
                        placeholder='8-xxx-xxx-xxxx *'
                        ref={register({
                            required: 'Укажите телефон',
                            pattern: {
                                value:/^[8]-[0-9]{3}-[0-9]{3}-[0-9]{4}/,
                                message: 'Формат телефона: 8-ххх-ххх-хххх'
                            },
                        })}
                    />
                    {errors.tel && <p className='errorMessage'>{errors.tel.message}</p>}
                    <button
                        type='submit'
                        onClick={handleSubmit(onSubmit)}>
                            Зарегистрироваться
                    </button>
                </form>
            </div>
        );
}

export default Registration;

