import styled from "styled-components";
import img from "./images/bg1.jpeg";

export const Container = styled.div`
  background-image: url(${img});
  padding: 10px 30px 20px;
  background-size: contain;
`

export const BlockBasket = styled.div`
  display: flex;
  justify-content: space-between;
  position: fixed;
  width: 95%;
  background-color: oldlace;
  height: 50px;
  align-items: center;
`

export const SearchLine = styled.div`
  padding-left: 15px;
`

export const WindowSearch = styled.input`
  height: 30px;
  width: 245px;
`

export const LoopButton = styled.button`
  width: 31px;
  height: 31px;
  margin-right: 34px;
  background-color: ${props => props.white === true ? 'white' : 'black'};
`

export const Loop = styled.img`
  height: 13px;
  width: 15px;
`

export const SortList = styled.select`
  height: 30px;
  width: 136px;
  margin-left: 13px;
`

export const BasketLine = styled.div`
  padding-right: 15px;
`

export const Basket = styled.img`
  height: 30px;
  width: 30px;
`

export const BasketNum = styled.span`
  position: absolute;
  right: 45px;
  top: 17px;
  color: whitesmoke;
  font-weight: bold;
  background-color: darkred;
  border-radius: 60px;
  width: 22px;
  height: 24px;
  text-align: center;
`

export const ShopGoods = styled.div`
  margin-top: 60px;
`

export const Title = styled.div`
  font-family: "Arial Black",serif;
  font-size: 25px;
  
`

export const Image = styled.img`
  width: 250px;
  height: 300px;
  border: 3px solid transparent;
  &:hover {
    border: 3px solid;
  }
`

export const Description = styled.div`
  white-space: pre-line;
`

export const QuantityGoods = styled.div`
  display: flex;
  justify-content: space-around;
  width: 86px;
  flex-direction: row-reverse;
  margin-top: 20px;
`

export const DecrIncrButton = styled.button`
  height: 30px;
  width: 30px;
`

export const PriceInfo = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
`
