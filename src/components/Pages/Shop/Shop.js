import React, {Component} from 'react';
import {connect} from "react-redux";
import {incrementItem, setBasket, decrementItem, sortItems, searchItem} from "../../../ducks/shop";
import {
    BasketLine, Basket, BasketNum,
    BlockBasket,
    Container, DecrIncrButton, Description, Image, Loop,
    LoopButton, PriceInfo, QuantityGoods,
    SearchLine,
    ShopGoods,
    SortList, Title,
    WindowSearch
} from "./shopStyles";


class Shop extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sort: 'desc',
            search: ''
        }
    }

    changeSort = ({target}) => {
        const {sortItems} = this.props;
        this.setState({sort: target.value})
        sortItems(target.value)
    }

    pressEnter = (e) => {
        const {search} = this.state;
        const {searchItem} = this.props;
        if (e.key === 'Enter') {
            searchItem(search)
        }
    }

    render() {
        const {history, searchItem, items, decrementItem, basket, incrementItem, setBasket} = this.props;
        const {sort, search} = this.state;
        return (
            <Container>
                <BlockBasket>
                    <SearchLine>
                        <WindowSearch onChange={e => this.setState({search: e.target.value})}
                           onKeyPress={this.pressEnter}
                           type='text' placeholder='Search...'
                        />
                        <LoopButton white={false} onClick={() => searchItem(search)}>
                            <Loop src={require('./images/loop.png')}/>
                        </LoopButton>
                        Сортировать по:
                        <SortList onChange={this.changeSort} value={sort}>
                            <option value='asc'>По возрастанию</option>
                            <option value='desc'>По убыванию</option>
                        </SortList>
                    </SearchLine>
                    <BasketLine>
                        <button onClick={() => history.push("/basket")}>
                            <Basket src={require('./images/shopCart.jpeg')}/>
                        </button>
                        <BasketNum>{basket.length}</BasketNum>
                    </BasketLine>
                </BlockBasket>
                {items.map((good, index) => (
                    <ShopGoods key={good.id}>
                        <Title>{good.title}</Title>
                        <Image onClick={() => history.push(`/shop/${good.id}`)} src={require('.' + good.img)}/>
                        <Description>
                            <h6>Системные требывания:</h6>{good.Description}
                        </Description>
                        <QuantityGoods>
                            <DecrIncrButton onClick={() => decrementItem(index)}>-</DecrIncrButton>
                            <div>{good.counts}</div>
                            <DecrIncrButton onClick={() => incrementItem(index)}>+</DecrIncrButton>
                        </QuantityGoods>
                        <PriceInfo>
                            <div>Цена: {good.price}</div>
                            <div> Валюта: {good.currency}</div>
                        </PriceInfo>
                        <button onClick={() => setBasket(index)}>Добавить в корзину</button>
                    </ShopGoods>

                ))}
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.shop.items,
        basket: state.shop.basket
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        incrementItem: (index) => dispatch(incrementItem(index)),
        decrementItem: (index) => dispatch(decrementItem(index)),
        sortItems: (sort) => dispatch(sortItems(sort)),
        searchItem: (search) => dispatch(searchItem(search)),
        setBasket: (index) => dispatch(setBasket(index)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop);