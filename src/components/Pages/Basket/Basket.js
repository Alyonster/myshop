import React, {Component} from "react";
import './basket.css'
import {connect} from "react-redux";
import {incrementBasketItem, decrementBasketItem} from "../../../ducks/shop";

class Basket extends Component {

    renderSum = () => {
        const {basket} = this.props;
        let sum = 0;
        basket.forEach(item => {
            sum = sum + (item.price * item.counts)
        })
        return sum;
    }

    sumCounts = () => {
        const {basket} = this.props;
        let count = 0;
        basket.forEach(item => {
            count = count + item.counts
        })
        return count;
    }

    render() {
        const {basket, incrementBasketItem, decrementBasketItem} = this.props;
        return (
            basket.length > 0 ? <div className='bg'>
                {basket.map((good, index) => (
                    <div>
                        <div className='title'>{good.title}</div>
                        <img alt='' className='image' src={require('.' + good.img)}/>
                        <div className='description'>Системные требывания:{good.Description}</div>
                        <div className='quantityGoods'>
                            <button className='incrDecrButton' onClick={() => decrementBasketItem(index)}>-</button>
                            <div>{good.counts}</div>
                            <button className='incrDecrButton' onClick={() => incrementBasketItem(index)}>+</button>
                        </div>
                        <div className = 'price'>Цена: {good.price}</div>
                        <div className = 'currency'>  Валюта: {good.currency}</div>
                    </div>
                ))}
                <div className='totalList'>
                    <div>Итого сумма: {this.renderSum()}</div>
                    <div>Товаров: {this.sumCounts()}</div>
                </div>
            </div> : <div className='basketEmpty'>
                        <h2>Ваша корзина пуста=(</h2>
                    </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        basket: state.shop.basket
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        incrementBasketItem: (index) => dispatch(incrementBasketItem(index)),
        decrementBasketItem: (index) => dispatch(decrementBasketItem(index)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
