import {applyMiddleware, combineReducers, compose, createStore} from 'redux'
import thunkMiddleware from 'redux-thunk'
import shop from "../ducks/shop";


const middleware = [];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

middleware.push(thunkMiddleware);

export default createStore(
    combineReducers({shop}),
    composeEnhancers(applyMiddleware(...middleware))
);