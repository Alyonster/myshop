import React from 'react';
import ReactDOM from 'react-dom';
import Routing from "./Routing";
import {Provider} from "react-redux";
import Store from "./store";
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <Provider store={Store}>
        <Routing/>
    </Provider>,
    document.getElementById('root')
);