import {BrowserRouter as Router, Redirect, Route, Switch, useHistory} from 'react-router-dom'
import Login from "./components/Pages/Login/Login";
import React from 'react'
import Shop from "./components/Pages/Shop/Shop";
import Registration from "./components/Pages/Login/Registration";
import ShopItemBody from "./components/Pages/ShopItemBody/ShopItemBody";
import Basket from "./components/Pages/Basket/Basket";


export default function Routing() {
    return (
        <Router history={useHistory()}>
            <Switch>
                <Route component={Login} path='/login'/>
                <Route component={Shop} path='/shop' exact/>
                <Route component={Registration} path='/registration'/>
                <Route component={Basket} path='/basket'/>
                <Route component={ShopItemBody} path='/shop/:itemId'/>
                <Redirect to='/login'/>
            </Switch>
        </Router>
    )
}